'''
The main vending machine module
'''


class Machine(object):
    '''
    Vending Machine class
    '''
    def __init__(self):
        self.items = {
            'button1': 'cola',
            'button2': 'chips',
            'button3': 'candy',
        }
        self.prices = {
            'cola': 1.00,
            'chips': 0.50,
            'candy': 0.65,
        }
        self.inventory = {
            'cola': 10,
            'chips': 10,
            'candy': 10,
            'quarters': 10,
            'dimes': 10,
            'nickles': 10,
        }

        self.display = ''
        self.inserted_money = 0.00
        self.coin_return = 0.00

    @staticmethod
    def identify(weight, diameter):
        '''
        Idenify the coin based on weight and diameter
        '''
        if (weight, diameter) == (5.000, 21.21):
            return 0.05
        elif (weight, diameter) == (2.268, 17.91):
            return 0.10
        elif (weight, diameter) == (5.670, 30.61):
            return 0.25
        elif (weight, diameter) == (2.500, 19.05):
            return 0.01
        else:
            return 0

    def insert(self, weight=None, diameter=None):
        '''
        Simulates a customer inserting a coin and only knowing its size and
        mass
        '''
        coin_value = self.identify(weight=weight, diameter=diameter)
        if coin_value in (0.10, 0.05, 0.25):
            self.inserted_money += coin_value
            if coin_value == 0.05:
                self.inventory['nickles'] += 1
            elif coin_value == 0.10:
                self.inventory['dimes'] += 1
            elif coin_value == 0.25:
                self.inventory['quarters'] += 1
        else:
            self.coin_return += coin_value

    def remove_coins(self, change):
        '''
        Ensures the inventory of coins is accurate when dispensing change
        '''
        while round(change, 2) > 0:
            if round(change, 2) >= 0.25 and self.inventory['quarters'] > 0:
                self.inventory['quarters'] -= 1
                change -= 0.25
            elif round(change, 2) >= 0.10 and self.inventory['dimes'] > 0:
                self.inventory['dimes'] -= 1
                change -= 0.10
            elif round(change, 2) >= 0.05 and self.inventory['nickles'] > 0:
                self.inventory['nickles'] -= 1
                change -= 0.05
            elif self.inventory['nickles'] == 0:
                raise Exception("Ran out of money")

    def press_coin_return(self):
        '''
        Simulates the customer pressing the coin return
        '''
        self.remove_coins(self.inserted_money)
        self.coin_return += self.inserted_money
        self.inserted_money = 0

    def state(self):
        '''
        Show the current display output
        '''
        if self.display:
            display, self.display = self.display, ''
            return display
        elif self.inserted_money:
            return '{:.2f}'.format(self.inserted_money)
        elif not self.is_able_to_make_change():
            return "EXACT CHANGE"
        else:
            return "INSERT COIN"

    def is_able_to_make_change(self):
        '''
        Represents if the machine still has enough nickles and dimes to make
        change
        '''
        return self.inventory['nickles'] > 0 and (
            self.inventory['dimes'] > 0 or self.inventory['nickles'] > 2)

    def select_item(self, button):
        '''
        Simulates the customer pressing a button
        '''
        item = self.items[button]
        cost = self.prices[item]
        if self.inventory[item] == 0:
            self.display = 'SOLD OUT'
        elif cost > self.inserted_money:
            self.display = 'PRICE {:.2f}'.format(cost)
        else:
            self.inventory[item] -= 1
            self.inserted_money -= cost
            self.press_coin_return()
            self.display = "THANK YOU"


if __name__ == '__main__':
    pass
