'''
Tests for vending machine module
'''
import unittest
import vending_machine


class TestMachine(unittest.TestCase):
    def setUp(self):
        self.machine = vending_machine.Machine()

    def test_machine_display_on(self):
        display = self.machine.state()
        self.assertEqual(display, 'INSERT COIN')

    def test_machine_insert_coin_nickle(self):
        self.insert_coin('nickle')
        display = self.machine.state()
        self.assertEqual(display, "0.05")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_insert_coin_dime(self):
        self.insert_coin('dime')
        display = self.machine.state()
        self.assertEqual(display, "0.10")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_insert_quarter(self):
        self.insert_coin('quarter')
        display = self.machine.state()
        self.assertEqual(display, "0.25")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_insert_two_coins(self):
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        display = self.machine.state()
        self.assertEqual(display, "0.50")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_insert_invalid(self):
        self.insert_coin('penny')
        display = self.machine.state()
        self.assertEqual(display, "INSERT COIN")
        self.assertEqual(self.machine.coin_return, 0.01)

    def test_machine_return_coins(self):
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.machine.press_coin_return()
        display = self.machine.state()
        self.assertEqual(display, "INSERT COIN")
        self.assertEqual(self.machine.coin_return, 0.50)

    def test_machine_buy_with_exact_change(self):
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.buy_item('cola')
        display = self.machine.state()
        self.assertEqual(display, "THANK YOU")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_show_price_without_enough_money(self):
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.buy_item('cola')
        display = self.machine.state()
        self.assertEqual(display, "PRICE 1.00")
        display = self.machine.state()
        self.assertEqual(display, "0.75")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_show_price_without_money(self):
        self.buy_item('cola')
        display = self.machine.state()
        self.assertEqual(display, "PRICE 1.00")
        display = self.machine.state()
        self.assertEqual(display, "INSERT COIN")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_buy_with_inexact_change(self):
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.insert_coin('quarter')
        self.buy_item('candy')
        display = self.machine.state()
        self.assertEqual(display, "THANK YOU")
        display = self.machine.state()
        self.assertEqual(display, "INSERT COIN")
        self.assertAlmostEqual(self.machine.coin_return, 0.10, 5)

    def test_machine_sold_out(self):
        for _ in range(11):
            self.insert_coin('quarter')
            self.insert_coin('quarter')
            self.insert_coin('quarter')
            self.insert_coin('quarter')
            self.buy_item('cola')
        display = self.machine.state()
        self.assertEqual(display, "SOLD OUT")
        self.assertEqual(self.machine.coin_return, 0.00)

    def test_machine_exact_change_only(self):
        for _ in range(10):
            for _ in range(7):
                self.insert_coin('dime')
            self.buy_item('candy')
            display = self.machine.state()
            self.assertEqual(display, "THANK YOU")
        for _ in range(7):
            self.insert_coin('dime')
        self.buy_item('candy')
        display = self.machine.state()
        self.assertEqual(display, "SOLD OUT")
        self.machine.press_coin_return()
        display = self.machine.state()
        self.assertEqual(display, "EXACT CHANGE")

    def insert_coin(self, name):
        '''
        Helper method to insert coin
        '''
        if name == 'quarter':
            self.machine.insert(weight=5.670, diameter=30.61)
        elif name == 'dime':
            self.machine.insert(weight=2.268, diameter=17.91)
        elif name == 'nickle':
            self.machine.insert(weight=5.0, diameter=21.21)
        elif name == 'penny':
            self.machine.insert(weight=2.500, diameter=19.05)

    def buy_item(self, name):
        '''
        Helper method to buy item
        '''
        if name == 'cola':
            self.machine.select_item('button1')
        elif name == 'chips':
            self.machine.select_item('button2')
        elif name == 'candy':
            self.machine.select_item('button3')


class TestCoinSorter(unittest.TestCase):
    def setUp(self):
        self.sorter = vending_machine.Machine()

    def test_identify_nickle(self):
        coin = self.sorter.identify(weight=5.0, diameter=21.21)
        self.assertEqual(coin, 0.05)

    def test_identify_dime(self):
        coin = self.sorter.identify(weight=2.268, diameter=17.91)
        self.assertEqual(coin, 0.10)

    def test_identify_quarter(self):
        coin = self.sorter.identify(weight=5.670, diameter=30.61)
        self.assertEqual(coin, 0.25)

    def test_identify_penny(self):
        coin = self.sorter.identify(weight=2.500, diameter=19.05)
        self.assertEqual(coin, 0.01)

    def test_identify_other(self):
        coin = self.sorter.identify(weight=2.768, diameter=17.91)
        self.assertEqual(coin, 0)


if __name__ == '__main__':
    unittest.main()
